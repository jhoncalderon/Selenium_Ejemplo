
package com.mycompany.seleniumejemplo;

import java.util.concurrent.TimeUnit;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author jhon.calderon7
 */
public class appejemploIT {
     
            
     public static  WebDriver driver= null;
            
            
    public appejemploIT() {
    }
    
    //Realice esto antes de ejecutar el test
    @BeforeClass
    public static void iniciar()
    {
       driver = new FirefoxDriver(); //Instanciamos el browser a utilizar
       driver.manage().window().maximize(); //maxime la ventana
       driver.get("http://www.google.com"); //URL a ir
       driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS); //Tiempo de espera para cargar la URL
    }
    
    //Ejecutar despues de ejecutar el Test
    @AfterClass
    public static void cerrardriver()
    {
        //driver.quit();        
    }
    
    //Test a ejecutar
    @Test
    public void flujoselenium() {
        
                     
        System.setProperty("webdriver.gecko.driver","C:\\geckodriver\\geckodriver.exe");             
        
        
        WebElement web= driver.findElement(By.id("lst-ib")); //Elemento en el que vamos a ingresar el texto
        String dato_a_buscar = "Seleniumhq"; //Texto a buscar en el navegador
        
        web.sendKeys(dato_a_buscar); //  Ingresamos el texto a buscar en el navegador        
              
        assertEquals("Seleniumhq", dato_a_buscar);              
        web.submit();//Clic en el boton para realizar la busqueda
        
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS); //Tiempo de espera para cargar la pagina
        
        driver.findElement(By.linkText("Selenium - Web Browser Automation")).click(); //Click en el primer link
        
        //driver.findElement(By.partialLinkText("Selenium - Web Browser Automation")).click();
        // driver.findElement(By.xpath("//*[contains(text(),'Selenium - Web Browser Automation')]")).click();
               
       assertEquals("Selenium - Web Browser Automation", driver.getTitle()); //Validamos el texto       
                     
    }       
}
